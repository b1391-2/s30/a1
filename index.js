const express = require("express");
const app = express();
const PORT = 4000;

//Require mongoose

const mongoose = require("mongoose");

//Middleware

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Connecting our server to mongodbatlas using mongoose
mongoose.connect("mongodb+srv://admin:admin@cluster0.qtkm7.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

//Get notification if successfully connected or not
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


//Schema
const userSchema = new mongoose.Schema(
	{
		username : String,
		password : String
	}
);

//Models
const userframe = mongoose.model("userframe", userSchema);

//Route

app.post("/signup", (request, response) => {
	//Link is working
	//console.log(`End line working properly`);
	//Can be console logged
	//console.log(request.body);
	//Working
	//response.send(request.body);

	//response.send(request.body.username);
	let registerUser = new userframe({
		username : request.body.username,
		password : request.body.password
	})

		registerUser.save((error, result)=>{
		console.log(result);
		return response.send(`${request.body.username} has registered successfully`);
	});
});

app.listen(PORT, () => console.log(`Server is working at Port: ${PORT}.`))